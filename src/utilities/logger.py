from datetime import datetime
import os

class DataLogger:

    def __init__(self):
        self._basepath = os.getcwd()
        now = datetime.now()
        self._now = now.strftime("%Y-%m-%d_%H%M%S")
        pass


    def log(self, memory, data):
        
        with open('{b}/test_logs/{m}/{n}.txt'.format(b=self._basepath, m=memory, n=self._now),'a') as w:
            w.write('{d}\n'.format(d=data))
        
            


