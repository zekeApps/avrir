import threading
import subprocess
from utilities.processid import get_pid
from time import sleep


class Avarice:

    def __init__(self, port):
        self._port = port
        self._started = False     
        #self.w = threading.Thread(target=self.__launch_avarice)   
         
    def start_avarice(self):
        """
        starts avr-gdb session, in case Connection is refused,
        It launches the avarice service in a separete thread
        :return: NA
        """


        
        if not self.check_status():
            self.w = threading.Thread(target=self.__launch_avarice)
            # self.w.setDaemon(True)
            self.w.start()
        else:
            print("Server is already running at pid: {p}".format(p=get_pid('avarice')))
        
              

    def __launch_avarice(self):
        print("starting up Avarice Server...")
        self._started = True
        subprocess.call(
            ['avarice', '--edbg', '--ignore-intr', ':{p}'.format(p=self._port)])
        # The w process will get blocked until released
        print("\n\n--> Avarice Server is now Offline")


    def check_status(self):
        if self._started:
            return self.w.is_alive()
        else:
            return False


    def close_avarice(self):
        pid = get_pid('avarice')
        if pid is not None:
            subprocess.call(['kill', '-9', '{pid}'.format(pid=pid)])
        else:
            print("Server is not running")




