from time import sleep
from pygdbmi.gdbcontroller import GdbController


class AvrGdb:

    def __init__(self, port, host="localhost"):
        self._host = host
        self._port =port
        self.gdbid= None
        self.res = None
        self.connected = False


    def open_avrgdb(self):
        if not self.connected:
            self.gdbid = GdbController('avr-gdb')
            self.gdbid.verbose = True
            self.res =  self.gdbid.write('target remote localhost:{p}'.format(p=self._port))
            
            for pay in self.res:
                if type(pay['payload']) is str:
                    print(pay)
                    if 'Remote debugging using' in pay['payload']:
                        self.connected = True
                        return True
            
            self.connected = False
            self.gdbid.exit()
            return False
        else:
            print("Already connected")

    def close_avrgdb(self):
        self.connected = False
        self.gdbid.exit()


    def check_status(self):
        return self.connected


    def _write(self, cmd):
        self.res =  self.gdbid.write(cmd)

        return self.res



    def set_data(self, address, val):

        data = self.gdbid.write('set *{add}={val}'.format(add=address, val=val))

        if data[2]['message'] is 'error':
            return (False, data[2]['payload']['msg'])

        else:
            return(True, data[2]['message'])



    def get_data(self, address):
        self.res = self.gdbid.write('x/xh {add}'.format(add=address))
        data = self.res[1]['payload']
        address = data[:data.find(':')]
        val = data[data.find('0x', len(address)):data.find('\\n')]
        if val is '':
            val = None
        return(address, val)


