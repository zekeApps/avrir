import subprocess

class AvrDude:

    def __init__(self, mcu):
        self._mcu = '{m}'.format(m=mcu)
        self._device = 'atmelice'

    def save_flash(self):
        return self._avrdude('flash:r:./bin/flash.hex:i')
        
    def save_eeprom(self):
        return self._avrdude('eeprom:r:./bin/eeprom.hex:i')

    def save_lfuses(self):
        return self._avrdude('lfuse:r:./bin/lfuses.hex:i')

    def save_hfuses(self):
        return self._avrdude('hfuse:r:./bin/hfuses.hex:i')

    def save_efuses(self):
         return self._avrdude('efuse:r:./bin/efuses.hex:i')
       
    def save_lock(self):
        return self._avrdude('lock:r:./bin/lock.hex:i')

    def save_calibration(self):
        return self._avrdude('calibration:r:./bin/calibration.hex:i')

    def save_signature(self):
        return self._avrdude('signature:r:./bin/signature.hex:i')

    def verify_flash(self):
        return self._avrdude('flash:v:./bin/flash.hex:i')

    def verify_eeprom(self):
        return self._avrdude('eeprom:v:./bin/eeprom.hex:i')

    def verify_lfuses(self):
        return self._avrdude('lfuse:v:./bin/lfuses.hex:i')

    def verify_hfuses(self):
         return self._avrdude('hfuse:v:./bin/hfuses.hex:i')

    def verify_efuses(self):
         return self._avrdude('efuse:v:./bin/efuses.hex:i')

    def verify_lock(self):
        return self._avrdude('lock:v:./bin/lock.hex:i')

    def verify_calibration(self):
        return self._avrdude('calibration:v:./bin/calibration.hex:i')

    def verify_signature(self):
        return self._avrdude('signature:v:./bin/signature.hex:i')

    def _avrdude(self, command):
        cmd = 'avrdude -p {mcu} -c {dev} -U {c}'.format(mcu=self._mcu, dev=self._device, c=command)
        # print(cmd)
        p = subprocess.Popen(cmd, stderr=subprocess.PIPE, shell=True)
        error = p.communicate()
        return (p.returncode, error)
