from utilities.avarice import Avarice
from colorama import Fore, Back, Style, init
from utilities.logger import DataLogger
from utilities.avrgdb import AvrGdb
from utilities.avrdude import AvrDude
from time import sleep

_init = 0
_flash_test = 1
_eeprom_test = 2
_hfuses_test = 3
_lfuses_test = 4
_efuses_test = 5
_lock_test = 6
_calibration_test = 7
_signature_test = 8

_start_server = 10
_start_gdb = 11 
_open_gdb = 12
_test_ram = 13
_stop_gdb = 14
_stop_avarice = 15

_end = 254
_error = 255

_isram_start = 0x800100
_isram_end = 0x8010FF
_patterns = [0x0000, 0xFFFF]


_max_trials = 3

state = _init

if __name__ == '__main__':
    
    init(autoreset=True)
    _cycles = 0
    _port = 4242
    server = Avarice(_port)
    gdb = AvrGdb(_port)
    l = DataLogger()
    dude = AvrDude('c64')
    

    while(True):

        if state is _init:
            error_code = 0
            _trials = 0

            state = _flash_test

        elif state is _flash_test:
            print('cycles:', _cycles)
            print(Fore.YELLOW + Back.BLUE + "---------Flash Memory Test--------")
            data = dude.verify_flash()
            if data[0] is not 0:
                print(Fore.WHITE + Back.RED + "-->FAILED")
                l.log("FLASH", data[1])
            else:
                print(Fore.WHITE + Back.GREEN + "-->PASSED")
            state = _eeprom_test

        elif state is _eeprom_test:
            print(Fore.YELLOW + Back.BLUE + "---------EEPROM Memory Test--------")
            data = dude.verify_eeprom()
            if data[0] is not 0:
                print(Fore.WHITE + Back.RED + "-->FAILED")
                l.log("EEPROM", data[1])
            else:
                print(Fore.WHITE + Back.GREEN + "-->PASSED")
            state = _hfuses_test

        elif state is _hfuses_test:
            print(Fore.YELLOW + Back.BLUE + "---------HighFUSE Memory Test--------")
            data = dude.verify_hfuses()
            if data[0] is not 0:
                print(Fore.WHITE + Back.RED + "-->FAILED")
                l.log("HFUSE", data[1])
            else:
                print(Fore.WHITE + Back.GREEN + "-->PASSED")
            state = _lfuses_test

        elif state is _lfuses_test:
            print(Fore.YELLOW + Back.BLUE + "---------LowFUSE Memory Test--------")
            data = dude.verify_lfuses()
            if data[0] is not 0:
                print(Fore.WHITE + Back.RED + "-->FAILED")
                l.log("LFUSE", data[1])
            else:
                print(Fore.WHITE + Back.GREEN + "-->PASSED")
            state = _efuses_test

        elif state is _efuses_test:
            print(Fore.YELLOW + Back.BLUE + "---------eFUSE Memory Test--------")
            data = dude.verify_efuses()
            if data[0] is not 0:
                print(Fore.WHITE + Back.RED + "-->FAILED")
                l.log("EFUSE", data[1])
            else:
                print(Fore.WHITE + Back.GREEN + "-->PASSED")
            state = _lock_test

        elif state is _lock_test:
            print(Fore.YELLOW + Back.BLUE + "---------LOCK Memory Test--------")
            data = dude.verify_lock()
            if data[0] is not 0:
                print(Fore.WHITE + Back.RED + "-->FAILED")
                l.log("LOCK", data[1])
            else:
                print(Fore.WHITE + Back.GREEN + "-->PASSED")
            state = _calibration_test

        elif state is _calibration_test:
            print(Fore.YELLOW + Back.BLUE + "---------CALIBRATION Memory Test--------")
            data = dude.verify_calibration()
            if data[0] is not 0:
                print(Fore.WHITE + Back.RED + "-->FAILED")
                l.log("CALIBRATION", data[1])
            else:
                print(Fore.WHITE + Back.GREEN + "-->PASSED")
            state = _signature_test

        elif state is _signature_test:
            print(Fore.YELLOW + Back.BLUE + "---------SIGNATURE Memory Test--------")
            data = dude.verify_signature()
            if data[0] is not 0:
                print(Fore.WHITE + Back.RED + "-->FAILED")
                l.log("SIGNATURE", data[1])
            else:
                print(Fore.WHITE + Back.GREEN + "-->PASSED")

            if not server.check_status():
                # start the server
                state = _start_server
            else:
                # Server is up and running
                state = _open_gdb

        elif state is _start_server:
            server.start_avarice()
            if server.check_status():
                print(Fore.GREEN + 'Server is up and running')
                state =  _open_gdb
            else:
                print("Server is not available")
                _trials = _trials + 1
                if _trials >= _max_trials:
                    print("Server could not be launched")
                    state = _error
                    error_code = 1

        elif state is _open_gdb:
            sleep(2)
            if not gdb.check_status():
                gdb.open_avrgdb()
                print(Fore.GREEN + "avr-gdb is now connected to port: {p}".format(p=_port))
            else:
                print(Fore.YELLOW + "avr-gdb was already connected")

            if gdb.check_status():
                state = _test_ram
            else:
                state = _error
                error = 2

        elif state is _test_ram:
            print(Fore.YELLOW + Back.BLUE + "---------SRAM test--------")
            for pattern in _patterns:
                print(Fore.BLACK + Back.CYAN + 'Pattern: 0x{p:04x}'.format(p=pattern))
                for i in range(_isram_start, _isram_end + 1):
                    gdb.set_data(i, pattern)
                    val = gdb.get_data(i)
                
                    if not pattern == int(val[1], 16):
                        print(Fore.YELLOW + '0x{a:04x}:0x{v:04x}'.format(a=i, v=int(val[1], 16))+Fore.RED + ' Failed')
                        l.log('SRAM', 'address->0x{a:04x}\tpattern->0x{p:04x}\tread:0x{val:04x}'.
                              format(a=i, p=pattern, val=int(val[1], 16)))
                    else:
                        if i%2:
                            print(Fore.YELLOW +'0x{a:04x}:0x{v:04x}'.format(a=i,v=pattern)+Back.GREEN + Fore.WHITE + ' PASSED')
                        else:
                            print(Fore.YELLOW +'0x{a:04x}:0x{v:04x}'.format(a=i,v=pattern)+Fore.GREEN + ' PASSED')
                    print('\x1b[2A')
                print('\x1b[1B')    
            print(Fore.BLACK + Back.WHITE + "------->done")
            state = _stop_gdb 
        
        elif state is _stop_gdb:
            gdb.close_avrgdb()
            state = _stop_avarice 

        elif state is _stop_avarice:
            server.close_avarice()
            _cycles = _cycles + 1
            state = _init

        elif  state is _error:
            print("Error")
            state = _end

        elif state is _end:
            break








                
        







